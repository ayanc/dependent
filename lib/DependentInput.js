'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.mapStateToProps = exports.DependentInputComponent = undefined;

var _toConsumableArray2 = require('babel-runtime/helpers/toConsumableArray');

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _objectWithoutProperties2 = require('babel-runtime/helpers/objectWithoutProperties');

var _objectWithoutProperties3 = _interopRequireDefault(_objectWithoutProperties2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRedux = require('react-redux');

var _reduxForm = require('redux-form');

var _lodash = require('lodash.get');

var _lodash2 = _interopRequireDefault(_lodash);

var _FormField = require('admin-on-rest/lib/mui/form/FormField');

var _FormField2 = _interopRequireDefault(_FormField);

var _getValue = require('./getValue');

var _getValue2 = _interopRequireDefault(_getValue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var REDUX_FORM_NAME = 'record-form';

var DependentInputComponent = function DependentInputComponent(_ref) {
    var children = _ref.children,
        show = _ref.show,
        dependsOn = _ref.dependsOn,
        value = _ref.value,
        resolve = _ref.resolve,
        props = (0, _objectWithoutProperties3.default)(_ref, ['children', 'show', 'dependsOn', 'value', 'resolve']);

    if (!show) {
        return null;
    }

    if (Array.isArray(children)) {
        return _react2.default.createElement(
            'div',
            null,
            _react2.default.Children.map(children, function (child) {
                return _react2.default.createElement(
                    'div',
                    {
                        key: child.props.source,
                        style: child.props.style,
                        className: 'aor-input-' + child.props.source
                    },
                    _react2.default.createElement(_FormField2.default, (0, _extends3.default)({ input: child }, props))
                );
            })
        );
    }

    return _react2.default.createElement(
        'div',
        { key: children.props.source, style: children.props.style, className: 'aor-input-' + children.props.source },
        _react2.default.createElement(_FormField2.default, (0, _extends3.default)({ input: children }, props))
    );
};

exports.DependentInputComponent = DependentInputComponent;
DependentInputComponent.propTypes = {
    children: _propTypes2.default.node.isRequired,
    show: _propTypes2.default.bool.isRequired,
    dependsOn: _propTypes2.default.any,
    value: _propTypes2.default.any,
    resolve: _propTypes2.default.func,
    formName: _propTypes2.default.string
};

var mapStateToProps = exports.mapStateToProps = function mapStateToProps(state, _ref2) {
    var resolve = _ref2.resolve,
        dependsOn = _ref2.dependsOn,
        value = _ref2.value,
        _ref2$formName = _ref2.formName,
        formName = _ref2$formName === undefined ? REDUX_FORM_NAME : _ref2$formName;

    if (resolve && (dependsOn === null || typeof dependsOn === 'undefined')) {
        var values = (0, _reduxForm.getFormValues)(formName)(state);
        return { dependsOnValue: values, depends: dependsOn, show: resolve(values, dependsOn, value) };
    }

    var formValue = void 0;
    // get the current form values from redux-form
    if (Array.isArray(dependsOn)) {
        // We have to destructure the array here as redux-form does not accept an array of fields
        formValue = (0, _reduxForm.formValueSelector)(formName).apply(undefined, [state].concat((0, _toConsumableArray3.default)(dependsOn)));
    } else {
        formValue = (0, _reduxForm.formValueSelector)(formName)(state, dependsOn);
    }

    if (resolve) {
        return {
            dependsOnValue: formValue,
            depends: dependsOn,
            show: resolve(formValue, dependsOn)
        };
    }

    if (Array.isArray(dependsOn) && Array.isArray(value)) {
        return {
            dependsOnValue: formValue,
            depends: dependsOn,
            show: dependsOn.reduce(function (acc, s, index) {
                return acc && (0, _lodash2.default)(formValue, s) === value[index];
            }, true)
        };
    }

    if (typeof value === 'undefined') {
        if (Array.isArray(dependsOn)) {
            return {
                dependsOnValue: formValue,
                depends: dependsOn,
                show: dependsOn.reduce(function (acc, s) {
                    return acc && !!(0, _getValue2.default)(formValue, s);
                }, true)
            };
        }

        return { dependsOnValue: formValue, depends: dependsOn, show: !!formValue };
    }

    return { dependsOnValue: formValue, depends: dependsOn, show: formValue === value };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps)(DependentInputComponent);